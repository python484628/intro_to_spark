# -*- coding: utf-8 -*-

# Author : Marion Estoup
# E-mail : marion_110@hotmail.fr
# October 2021

# Install Pyspark and create a spark context then stop it

# Install pyspark
!pip install pyspark

# Create a spark context 
from pyspark import SparkContext

# If you already have a spark context running you can stop it by doing
# sc = SparkContext.getOrCreate()
# sc.stop()

# Define sparkcontext in sc variable
sc = SparkContext("local[2]", "First App")

# Stopping Spark context 
sc.stop()



# Define a spark configuration and a spark context
# Configuration 
# To use all capacity
# Import SParkConf and SParkContext
from pyspark import SparkConf, SparkContext

# Create conf variable
conf = (SparkConf()
  .setMaster("local[*]") 
  .setAppName("My app") 
  .set("spark.executor.memory", "1g"))

# Define sparkcontext in sc variable
sc = SparkContext(conf = conf)

# Inspect 
sc.pythonVer # retrieve python version
# Result 3.9

# Master url to connect to
sc.master 
# Result local[*]

# Path where psark is installed on worker nodes
str(sc.sparkHome)
# Result 'None'


# Retrieve name of the spark user running sparkcontext
str(sc.sparkUser()) 
# Result 'mario'

# Return application name
sc.appName 
# Result 'My app'

# Retrieve application ID 
sc.applicationId 
# Result 'local-1665485202284'

# Return defaut level of parallelism
sc.defaultParallelism 
# Result 8


# Default min number of partitions for RDDs
sc.defaultMinPartitions
# Result 2 


# How to do using shell
# Using the shell : in the pyspark shell, a special interpreter aware sparkcontext is already created in the variable called sc
# $./bin/spark-shell--master local[2]
# $./bin/pyspark--master local[4] --py-files code.py

# Set which master the context connects to with the --master argument, and add python .zip, .egg or .py files to the runtime path by passing comma-separated list to --py-files
# Execution
# $./bin/spark-submit examples/src/main/python/pi.py 

# Loading data with RDDs (resilient distributed datasets)


# Back to python to use spark rdd

# Pyspark RDDs resilient distributed datasets
# Loading data
# External data : read either one text file from HDFS, a local file system or any Haddop supported file system URI with textFile(), or read in a directory of text files with whole TextFiles()

textFile = sc.textFile("C:/Users/mario/Downloads/README (1).md")


# Export the first line that is in the RDD README.md
textFile.first()
# Result 'This directory includes a few sample datasets to get you started.'

# See what's in the RDD
textFile.collect()
# Result
# ['This directory includes a few sample datasets to get you started.',
# '',
# '*   `california_housing_data*.csv` is California housing data from the 1990 US',
# '    Census; more information is available at:',
# '    https://developers.google.com/machine-learning/crash-course/california-housing-data-description',
# '',
# '*   `mnist_*.csv` is a small sample of the',
# '    [MNIST database](https://en.wikipedia.org/wiki/MNIST_database), which is',
# '    described at: http://yann.lecun.com/exdb/mnist/',
# '',
# '*   `anscombe.json` contains a copy of',
# "    [Anscombe's quartet](https://en.wikipedia.org/wiki/Anscombe%27s_quartet); it",
# '    was originally described in',
# '',
# "    Anscombe, F. J. (1973). 'Graphs in Statistical Analysis'. American",
# '    Statistician. 27 (1): 17-21. JSTOR 2682899.',
# '',
# '    and our copy was prepared by the',
# '    [vega_datasets library](https://github.com/altair-viz/vega_datasets/blob/4f67bdaad10f45e3549984e17e1b3088c731503d/vega_datasets/_data/anscombe.json).']



# Word count with pySpark
# Count the occurences of unique words in a text 
# Split each line into words
# Create words variable
words = textFile.flatMap(lambda line: line.strip().split(" "))



# Collect the words that are in the sentences using words variable and collect()
words.collect()
# Result
# ['This',
 # 'directory',
 # 'includes',
 # 'a',
 # 'few',
 # 'sample',
 # 'datasets',
 # 'to',
 # 'get',
 # 'you',
 # 'started.',
 # '',
 # '*',
 # '',
 # '',
 # '`california_housing_data*.csv`',
 # 'is',
 # 'California',
 # 'housing',
 # 'data',
 # 'from',
 # 'the',
 # '1990',
 # 'US',
 # 'Census;',
 # 'more',
 # 'information',
 # 'is',
 # 'available',
 # 'at:',
 # 'https://developers.google.com/machine-learning/crash-course/california-housing-data-description',
 # '',
 # '*',
 # '',
 # '',
 # '`mnist_*.csv`',
 # 'is',
 #'a',
 # 'small',
 # 'sample',
 # 'of',
 # 'the',
 # '[MNIST',
 # 'database](https://en.wikipedia.org/wiki/MNIST_database),',
 # 'which',
 # 'is',
 # 'described',
 # 'at:',
 # 'http://yann.lecun.com/exdb/mnist/',
 # '',
 # '*',
 # '',
 # '',
 # '`anscombe.json`',
 # 'contains',
 # 'a',
 # 'copy',
 # 'of',
 # "[Anscombe's",
 # 'quartet](https://en.wikipedia.org/wiki/Anscombe%27s_quartet);',
 # 'it',
 # 'was',
 # 'originally',
 # 'described',
 # 'in',
 # '',
 # 'Anscombe,',
 # 'F.',
 # 'J.',
 # '(1973).',
 # "'Graphs",
 # 'in',
 # 'Statistical',
 # "Analysis'.",
 # 'American',
 # 'Statistician.',
 # '27',
 # '(1):',
 # '17-21.',
 # 'JSTOR',
 # '2682899.',
 # '',
 # 'and',
 # 'our',
 # 'copy',
 # 'was',
 # 'prepared',
 # 'by',
 # 'the',
 # '[vega_datasets',
 # 'library](https://github.com/altair-viz/vega_datasets/blob/4f67bdaad10f45e3549984e17e1b3088c731503d/vega_datasets/_data/anscombe.json).']




# Count the number of all words in the document
wordCounts = words.count()
print(wordCounts)
# Result 91

# Count the occurrence of each word stored in wordCounts variable
wordCounts = words.map(lambda word: (word, 1)).reduceByKey(lambda a,b:a +b)


# Show the result with collect() using wordCounts variable to the occurrence of each word
wordCounts.collect()
# Result
# [('directory', 1),
 # ('datasets', 1),
 # ('', 11),
 # ('*', 3),
 # ('`california_housing_data*.csv`', 1),
 # ('is', 4),
 # ('housing', 1),
 # ('1990', 1),
 # ('more', 1),
 # ('at:', 2),
 # ('https://developers.google.com/machine-learning/crash-course/california-housing-data-description',
 #  1),
 # ('`mnist_*.csv`', 1),
 # ('of', 2),
 # ("[Anscombe's", 1),
 # ('was', 2),
 # ('originally', 1),
 # ('in', 2),
 # ('Anscombe,', 1),
 # ('F.', 1),
 # ("'Graphs", 1),
 # ('American', 1),
 # ('Statistician.', 1),
 # ('(1):', 1),
 # ('prepared', 1),
 # ('This', 1),
 # ('includes', 1),
 # ('a', 3),
 # ('few', 1),
 # ('sample', 2),
 # ('to', 1),
 # ('get', 1),
 # ('you', 1),
 # ('started.', 1),
 # ('California', 1),
 # ('data', 1),
 # ('from', 1),
 # ('the', 3),
 # ('US', 1),
 # ('Census;', 1),
 # ('information', 1),
 # ('available', 1),
 # ('small', 1),
 # ('[MNIST', 1),
 # ('database](https://en.wikipedia.org/wiki/MNIST_database),', 1),
 # ('which', 1),
 # ('described', 2),
 # ('http://yann.lecun.com/exdb/mnist/', 1),
 # ('`anscombe.json`', 1),
 # ('contains', 1),
 # ('copy', 2),
 # ('quartet](https://en.wikipedia.org/wiki/Anscombe%27s_quartet);', 1),
 # ('it', 1),
 # ('J.', 1),
 # ('(1973).', 1),
 # ('Statistical', 1),
 # ("Analysis'.", 1),
 # ('27', 1),
 # ('17-21.', 1),
 # ('JSTOR', 1),
 # ('2682899.', 1),
 # ('and', 1),
 # ('our', 1),
 # ('by', 1),
 # ('[vega_datasets', 1),
 # ('library](https://github.com/altair-viz/vega_datasets/blob/4f67bdaad10f45e3549984e17e1b3088c731503d/vega_datasets/_data/anscombe.json).',
 #  1)]




# Parallelized collections
# Create rdd1, 2, 3 and 4
rdd1 = sc.parallelize([('a',7),('a',2),('b',2)])
rdd2 = sc.parallelize([('a',["x","y","z"]),('b',["p","r"])])
rdd3 = sc.parallelize(range(100))
rdd4 = sc.parallelize([('a',["x","y","z"]),('b',["p","r"])])


# Retrieving RDD information
rdd1.getNumPartitions() # List the number of partitions
# Result 8


# Count RDD instances in rdd1
rdd1.count() 
# Result 3


# Count RDD instances by key in rdd1
rdd1.countByKey() 
# Result defaultdict(int, {'a': 2, 'b': 1})


# Count RDD instance by value in rdd1
rdd1.countByValue() 
# Result defaultdict(int, {('a', 7): 1, ('a', 2): 1, ('b', 2): 1})

# Return (key,value) pairs as a dictionnary in rdd1
rdd1.collectAsMap() 
# Result {'a': 2, 'b': 2}


# Check wether RDD is empty
sc.parallelize([]).isEmpty() 
# Result True



# Summary of rdd3
# Sum of RDD elements
print(rdd3.sum()) 
# Result 4950

# Max value of RDD elements
print(rdd3.max()) 
# Result 99

# Min value of RDD elements
print(rdd3.min()) 
# Result 0

# Mean value of RDD elements
print(rdd3.mean()) 
# Result 49.5

# Standard deviation value of RDD elements
print(rdd3.stdev()) 
# Result 28.86607004772212

# Compute variance of RDD elements
print(rdd3.variance()) 
# Result 833.25

# Compute histogram by bins
print(rdd3.histogram(3)) 
# Result ([0, 33, 66, 99], [33, 33, 34])

# Sumarry statistics count, mean, stdev, max and min
print(rdd3.stats()) 
# Result (count: 100, mean: 49.5, stdev: 28.86607004772212, max: 99.0, min: 0.0)



# Applying functions in rdd1
rdd1.map(lambda x: x+(x[1], x[0])).collect() # Apply a function to each RDD element
# Result [('a', 7, 7, 'a'), ('a', 2, 2, 'a'), ('b', 2, 2, 'b')]


# Apply function and stored it in rdd5
rdd5 = rdd1.flatMap(lambda x: x+(x[1], x[0])) # apply a flatMap function to each RDD element and flatten the result
rdd5.collect()
# Result ['a', 7, 7, 'a', 'a', 2, 2, 'a', 'b', 2, 2, 'b']


# Apply function to rdd4
rdd4.flatMapValues(lambda x: x).collect() # Apply a flatMap function to each (key,value) pairs
# Result [('a', 'x'), ('a', 'y'), ('a', 'z'), ('b', 'p'), ('b', 'r')] 





## Selecting data

# Get elements of rdd1
rdd1.collect() # Return a list with all RDD elements
# Result [('a', 7), ('a', 2), ('b', 2)]



# First line in rdd1
rdd1.first() # Take first RDD element
# Result ('a', 7)



# Top 2 elements in rdd1
rdd1.top(2) # Take top 2 RDD elements
# Result [('b', 2), ('a', 7)]

# First 2 elements in rdd1
rdd1.take(2) # Take 2 first RDD elements
# Result [('a', 7), ('a', 2)]

# Sampling rdd3
rdd3.sample(True, 0.15, 81).collect() # Return sampled subset of rdd3
# Result [8, 8, 11, 12, 14, 17, 20, 33, 66, 69, 82, 83, 92, 93, 95]

# Filtering rdd1 
rdd1.filter(lambda x: "a" in x).collect() # Filter the RDD when it contains the letter a
# Result [('a', 7), ('a', 2)]

# Distinct values in rdd5
rdd5.distinct().collect() # Return distinct RDD values
# Result [2, 'b', 'a', 7]

# Keys in rdd1
rdd1.keys().collect() # Return  RDD's keys
# Result ['a', 'a', 'b']

# Values in rdd1
rdd1.values().collect() # Return RDD's values
# Result [7, 2, 2]

# Iterating in rdd1
def g(x): print(x)
rdd1.foreach(g) # Apply a function to all RDD elements
# Result
# ('b', 2)
# ('a', 2)
# ('a', 7)





## Reshaping Data


# Reducing in rdd1
rdd1.reduceByKey(lambda x,y : x+y).collect() # Merge the RDD values for each key
# Result [('a', 9), ('b', 2)]

# Merge rdd1 values
rdd1.reduce(lambda a,b: a + b) # Merge the RDD values
# Result ('a', 7, 'a', 2, 'b', 2)

# Grouping by in rdd3
rdd3.groupBy(lambda x: x % 2).mapValues(list).collect() # Return RDD of grouped values
# Result
# [(0,
#  [0,
#   2,
#   4,
#   6,
#   8,
#   10,
#   12,
#   14,
#   16,
#   18,
#   20,
#   22,
#   24,
#   26,
#   28,
#   30,
#   32,
#   34,
#   36,
#   38,
#   40,
#   42,
#   44,
#   46,
#   48,
#   50,
#   52,
  # 54,
  # 56,
  # 58,
  # 60,
  # 62,
 #  64,
 #  66,
 #  68,
 #  70,
 #  72,
 #  74,
 #  76,
 #  78,
 #  80,
 #  82,
 #  84,
 #  86,
 #  88,
 #  90,
 #  92,
 #  94,
 #  96,
#   98]),
# (1,
#  [1,
#   3,
#   5,
#   7,
 #  9,
 #  11,
 #  13,
 #  15,
 #  17,
 #  19,
 #  21,
 #  23,
 #  25,
 #  27,
 #  29,
 #  31,
 #  33,
 #  35,
 #  37,
 #  39,
 #  41,
 #  43,
 #  45,
 #  47,
 #  49,
 #  51,
 #  53,
 #  55,
 #  57,
 #  59,
 #  61,
 #  63,
 #  65,
 #  67,
 #  69,
 #  71,
 #  73,
 #  75,
 #  77,
 #  79,
 #  81,
 #  83,
 #  85,
 #  87,
 #  89,
 #  91,
 #  93,
  # 95,
  #  97,
   # 99])]

# Group by rdd key in rdd1
rdd1.groupByKey().mapValues(list).collect() # Group RDD by key
# Result [('a', [7, 2]), ('b', [2])]

# Aggregating
seqOp = (lambda x,y: (x[0]+y, x[1]+1))
combOp = (lambda x,y: (x[0]+y[0], x[1]+y[1]))

rdd1.aggregateByKey((0,0), seqOp,combOp).collect() # Aggregate values of each RDD key in rdd1
# Result [('a', (9, 2)), ('b', (2, 1))]

# Aggregate in rdd3
rdd3.aggregate((0,0),seqOp,combOp) # Aggregate RDD elements of each partition and then the results
# Result (4950, 100)

# Aggregate in rdd3
def add(x,y): return x+y
rdd3.fold(0,add) # Aggregate the elements of each partition, and then the results
# Result 4950

# Merge values for each key in rdd1
rdd1.foldByKey(0,add).collect() # Merge the values for each key
# Result [('a', 9), ('b', 2)]

# Create tuples of elements in rdd3
rdd3.keyBy(lambda x: x+x).collect() # Create tuples of RDD elements by applying a function
# Result
# [(0, 0),
#  (2, 1),
#  (4, 2),
#  (6, 3),
 # (8, 4),
 # (10, 5),
 # (12, 6),
 # (14, 7),
 # (16, 8),
 # (18, 9),
 # (20, 10),
 # (22, 11),
 # (24, 12),
 # (26, 13),
 # (28, 14),
 # (30, 15),
 # (32, 16),
 # (34, 17),
 # (36, 18),
 # (38, 19),
 # (40, 20),
 # (42, 21),
 # (44, 22),
 # (46, 23),
 # (48, 24),
 # (50, 25),
 # (52, 26),
 # (54, 27),
 # (56, 28),
 # (58, 29),
 # (60, 30),
 # (62, 31),
 # (64, 32),
 # (66, 33),
 # (68, 34),
 # (70, 35),
 # (72, 36),
 # (74, 37),
 # (76, 38),
 # (78, 39),
 # (80, 40),
 # (82, 41),
 # (84, 42),
 # (86, 43),
 # (88, 44),
 # (90, 45),
 # (92, 46),
 # (94, 47),
 # (96, 48),
 # (98, 49),
 # (100, 50),
 # (102, 51),
 # (104, 52),
 # (106, 53),
 # (108, 54),
 # (110, 55),
 # (112, 56),
 # (114, 57),
 # (116, 58),
 # (118, 59),
 # (120, 60),
 # (122, 61),
 # (124, 62),
 # (126, 63),
 # (128, 64),
 # (130, 65),
 # (132, 66),
 # (134, 67),
 # (136, 68),
 # (138, 69),
 # (140, 70),
 # (142, 71),
 # (144, 72),
 # (146, 73),
 # (148, 74),
 # (150, 75),
 # (152, 76),
 # (154, 77),
 # (156, 78),
 # (158, 79),
 # (160, 80),
 # (162, 81),
 # (164, 82),
 # (166, 83),
 # (168, 84),
 # (170, 85),
 # (172, 86),
 # (174, 87),
 # (176, 88),
 # (178, 89),
 # (180, 90),
 # (182, 91),
 # (184, 92),
 # (186, 93),
 # (188, 94),
 # (190, 95),
 # (192, 96),
 # (194, 97),
 # (196, 98),
 # (198, 99)]

# Mathematical Operations in rdd1
rdd1_1 = sc.parallelize([('a',4),('a',2),('b',1)])
print(rdd1.collect())
# Result [('a', 7), ('a', 2), ('b', 2)]

print(rdd1_1.collect())
# Result [('a', 4), ('a', 2), ('b', 1)]

print(rdd1.subtract(rdd1_1).collect())
# Result [('b', 2), ('a', 7)]

# parallelize and return key,value in rdd1
rdd1_1 = sc.parallelize([('a',4)])
rdd1.subtractByKey(rdd1_1).collect() # Return each (key,value) pair of rdd1 with no matching
# Result [('b', 2)]

# See what's in rdd1
print(rdd1.collect())
# Result [('a', 7), ('a', 2), ('b', 2)]

# See what's in rdd2
print(rdd2.collect())
# Result [('a', ['x', 'y', 'z']), ('b', ['p', 'r'])]

# Product of rdd1 and rdd2
rdd1.cartesian(rdd2).collect() # Return the cartesian product of rdd1 and rdd2
# Result
# [(('a', 7), ('a', ['x', 'y', 'z'])),
 # (('a', 7), ('b', ['p', 'r'])),
 # (('a', 2), ('a', ['x', 'y', 'z'])),
 # (('b', 2), ('a', ['x', 'y', 'z'])),
 # (('a', 2), ('b', ['p', 'r'])),
 # (('b', 2), ('b', ['p', 'r']))]

# Sort rdd2
rdd2.sortBy(lambda x: x[1]).collect() # Sort RDD by given function
# Result [('b', ['p', 'r']), ('a', ['x', 'y', 'z'])]

# Sort by key rdd2
rdd2.sortByKey().collect() # Sort (key,value) RDD by key
# [('a', ['x', 'y', 'z']), ('b', ['p', 'r'])]

# Repartitioning

rdd1.repartition(4) # New RDD with 4 partitions
# Result MapPartitionsRDD[132] at coalesce at <unknown>:0

# Use coalesce on rdd1
rdd1.coalesce(1) # Decrease the number of partitions in the RDD to 1
# Result CoalescedRDD[133] at coalesce at <unknown>:0


# Stop spark context
sc.stop()




## Pyspark DataFrames

# Initialyzing pyspark session

from pyspark.sql import SparkSession
spark = SparkSession.builder \
      .master("local[*]") \
      .appName("Cars app") \
      .config("spark.executor.memory", "1g") \
      .getOrCreate()

# Creating a pyspark dataframe
# Example 1
columns = ['ids','dogs','cats']
vals = [(1,2,0), (2,0,1)]
df = spark.createDataFrame(vals, columns)

# Show dataframe of example 1
df.show()
# +---+----+----+
# |ids|dogs|cats|
# +---+----+----+
# |  1|   2|   0|
# |  2|   0|   1|
# +---+----+----+

# Create another dataframe from other data sources
# Example 2
from pyspark.sql import Row
liste = [(1,2,2),(2,0,1)]
rdd2= map(lambda x: Row(ids=int(x[0]), dogs=int(x[1]), cats=int(x[2])), liste)
df2 = spark.createDataFrame(rdd2)
df2.show()
# +---+----+----+
# |ids|dogs|cats|
# +---+----+----+
# |  1|   2|   2|
# |  2|   0|   1|
# +---+----+----+

# Create dataframe example 3
from sklearn.datasets import load_breast_cancer
data = load_breast_cancer()
import pandas as pd
pdf = pd.DataFrame(data.data, columns = data.feature_names)

# Store datafrale of example 3 in variable df3
df3 = spark.createDataFrame(pdf)

# See df3
df3.show()


sc.stop()
















