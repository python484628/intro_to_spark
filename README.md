# Introduction_to_spark


This is an introduction to get to know basic commands with spark. There is a py file that was tested with spyder. To use it you will need to download the read me (1) file and put your path to use it. And there is an ipynb file that was tested with google colab. In the code you can find :



- Basic commands to install pyspark, create a spark context and configurate it (pip install pyspark, SparkContext,, SparkConf)



- Basic commands to know your environment (pythonVer, master, sparkHome, sparkUser, applicationId, defaultParallelism)



- Use and explore pyspark RDD (textFile, first, collect, flatmap, count, map, saveAsTextFile, parallelize,, getNumPartitions, countByKey, countByValue, collectAsMap, sum, max, min, mean, stdev, variance, histogram, stats, flatMapValues, top, take, sample, filter)



- Reshaping data (reduceByKey, reduce, groupBy, groupByKey, aggregateByKey, aggregate, fold, keyBy, parallelize, subtract, cartesian, sortBy, sortByKey, repartition, coalesce, saveAsTextFile)



- Pyspark dataframe (createDataFrame, map, read.csv)


Author : Marion Estoup

E-mail : marion_110@hotmail.fr

October 2021
